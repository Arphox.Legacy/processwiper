﻿using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ProcessWiper
{
    static class Logger
    {
        public static void Log()
        {
            switch (Config.LOGMODE)
            {
                case 0: return;
                case 1: _LogLocal(); break;
                case 2: _LogNetwork(); break;
            }
        }
        private static void _LogLocal()
        {
            List<string> titles = TitlesToLog();
            string logFileName = "log";

            File.AppendAllText(logFileName, "======================================================\r\n");
            File.AppendAllText(logFileName, DateTime.Now + "\r\n");
            File.AppendAllLines(logFileName, titles);
            File.AppendAllText(logFileName, "\r\n======================================================\r\n\r\n\r\n");
        }
        private static void _LogNetwork()
        {
            List<string> titles = TitlesToLog();
            if (titles.Count == 0) //If there are no running applications with a window
                return;

            //"machine name" directory
            string dirpath = Config.ADDRESS_LogDirectory + Environment.MachineName;
            if (!Directory.Exists(dirpath))
                Directory.CreateDirectory(dirpath);

            //today's logfile
            string filepath = dirpath + @"\" + DateTime.Today.ToString("yyyy.MM.dd.") + " (" + Environment.UserName + ").txt";
            if (!File.Exists(filepath))
                File.CreateText(filepath);

            File.AppendAllText(filepath, "======================================================\r\n");
            File.AppendAllText(filepath, DateTime.Now + "\r\n");
            File.AppendAllLines(filepath, titles);
            File.AppendAllText(filepath, "\r\n======================================================\r\n\r\n\r\n");
        }
        private static List<string> TitlesToLog()
        {
            Process[] processes = Process.GetProcesses();
            List<string> titles = new List<string>();

            foreach (Process p in processes)
            {
                if (!string.IsNullOrWhiteSpace(p.MainWindowTitle))
                {
                    string filename = "???";
                    try
                    {
                        filename = p.MainModule.FileName;
                    }
                    catch (Exception) { }
                    finally
                    {
                        if (!LogExceptionTitles.Contains(p.MainWindowTitle))
                        {
                            titles.Add(
                                string.Format("- {0}\t({1} - {2})",
                                p.MainWindowTitle,
                                p.ProcessName,
                                p.MainModule.FileName));
                        }
                    }
                }
            }
            return titles;
        }

        private static string[] LogExceptionTitles = new string[]
        //Doesn't log those applications, which names are EXACTLY like one of the strings here (==)
        {
            "Windows Shell Experience Host"
        };
    }
}