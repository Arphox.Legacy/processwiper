﻿using System;
using System.Linq;
using System.Windows.Threading;
using System.Diagnostics;

namespace ProcessWiper
{
    class ViewModel
    {
        DispatcherTimer mainTimer = new DispatcherTimer();
        DispatcherTimer logtimer = new DispatcherTimer();

        public ViewModel()
        {
            DataProvider.Init();

            mainTimer.Interval = Config.INTERVAL_ProcessCheck;
            mainTimer.Tick += ProcessCheck_TICK;
            mainTimer.Start();

            logtimer.Interval = Config.INTERVAL_Log;
            logtimer.Tick += Logtimer_Tick;
            logtimer.Start();
        }

        private void Logtimer_Tick(object sender, EventArgs e)
        {
            try
            {
                Logger.Log();
            }
            catch (Exception) { }
        }

        private void ProcessCheck_TICK(object sender, EventArgs ea)
        {
            try
            {
                var processes = Process.GetProcesses().Where(s => !s.ProcessName.Contains("svchost"));
                foreach (Process item in processes)
                {
                    CheckForProcessName(item);
                    CheckForTitle(item);
                }
            }
            catch (Exception) { }
        }

        private void CheckForProcessName(Process process)
        {
            foreach (string s in DataProvider.Processes)
            {
                if (process.ProcessName.ToLower().Contains(s.ToLower()))
                {
                    process.Kill();
                    break;
                }
            }
        }
        private void CheckForTitle(Process process)
        {
            foreach (string s in DataProvider.Titles)
            {
                if (process.MainWindowTitle.ToLower().Contains(s.ToLower()))
                {
                    process.Kill();
                    break;
                }
            }
        }
    }
}